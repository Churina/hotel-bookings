# Hotel Bookings Project Background:
Hotel Bookings, a hotel management software company, has approached Catalyte to build a frontend application for their reservations and room-types API. It needs to allow employees and managers at a hotel to add, edit, and delete reservations for guests, as well as let managers add and edit the types of room available for booking. You have access to the Hotel API code and can work off a local version during development, but the final application will be expected to work with the current deployed API as is.

# Hotel Bookings Frontend
* Run `npm install` in root folder to install dependencies.
* Run `npm start` and navigate to `localhost:3000` to view application.
* Run `npm test` to start the testing suite.
* Run `npm run coverage` to perform tests with coverage.

# Database
Ensure that your postgres database is available and configured with the following options:

* POSTGRES_USER=postgres
* POSTGRES_PASSWORD=root
* PORT=5432

The DataLoader class in the data package will load a few examples of each entity (Users, Reservation, Room Type) into the database after the service starts up.

# Hotel Bookings Backend
Download Hotel Bookings API, click "run" to start the application.