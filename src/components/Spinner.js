import React from 'react';

function Spinner() {
  return (
    <div className="loadingMsg">
      <div className="spinner" />
      <span>Loading... Please Wait!</span>
    </div>

  );
}

export default Spinner;
