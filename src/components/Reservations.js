import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Spinner from './Spinner';

function Reservations() {
  const [reservations, setReservations] = useState([]);
  const [roomType, setRoomType] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  useEffect(() => {
    setLoading(true);
    const getReservationsData = async () => {
      try {
        // fetch reservations
        const response = await fetch(
          'http://localhost:8080/reservations',
          {
            method: 'GET',
            headers:
          {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${sessionStorage.getItem('token')}`
          }
          }
        );

        const data = await response.json();
        setLoading(false);
        setReservations(data);

        // fetch room type
        const response1 = await fetch(
          'http://localhost:8080/room-types',
          {
            method: 'GET',
            headers:
          {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${sessionStorage.getItem('token')}`
          }
          }
        );

        const data1 = await response1.json();
        setLoading(false);
        setRoomType(data1);
      } catch (err) {
        if (!err.response) {
          setLoading(false);
          setError('Oops, something went wrong!');
        }
      }
    };
    getReservationsData();
  }, []);

  const handleDelete = async (id) => {
    setLoading(true);
    try {
      await fetch(
        `http://localhost:8080/reservations/${id}`,
        {
          method: 'DELETE',
          headers:
        {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${sessionStorage.getItem('token')}`
        }
        }
      );
      const deleteIndex = reservations.findIndex((reservation) => reservation.id === id);
      const reservationsNewList = [...reservations];
      reservationsNewList.splice(deleteIndex, 1);
      setLoading(false);
      setReservations(reservationsNewList);
    } catch (err) {
      if (!err.response) {
        setLoading(false);
        setError('Oops, something went wrong!');
      }
    }
  };

  return (
    <div className="reservation-roomType-container">
      <h1>Reservations</h1>
      {error && <p className="error-message">{error}</p>}
      {!error && <Link to="/reservations/create"><button type="button">Create</button></Link>}
      {loading && <Spinner pageStatus={loading} />}
      {reservations.length > 0 && roomType.length > 0 && reservations.map((reservation) => {
        const roomMatch = roomType.find((room) => room.id === reservation.roomTypeId);
        const matchRoomName = roomMatch.name;

        return (
          <div className="reservation-roomType-sub-container" key={reservation.id}>
            <div className="reservation-roomType-context">
              <p>
                Guest Email:
                {reservation.guestEmail}
              </p>
              <p>
                Room Type:
                {matchRoomName}
              </p>
              <p>
                Check-in Date:
                {reservation.checkInDate}
              </p>
              <p>
                Number of Night:
                {reservation.numberOfNights}
              </p>
              <p>
                Total Cost:
                {(roomMatch.rate * reservation.numberOfNights).toFixed(2)}
              </p>
            </div>

            <div className="button">
              <Link to={`/reservations/edit/${reservation.id}`}><button type="button">Edit</button></Link>
              <button type="button" onClick={() => handleDelete(reservation.id)}>Delete</button>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default Reservations;
