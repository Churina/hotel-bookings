import React from 'react';
import { NavLink } from 'react-router-dom';

function Navbar({ user }) {
  return (
    <nav>
      <h3>Hotel Bookings</h3>
      {user && <NavLink to="/reservations" className="navLink"> Reservations </NavLink>}
      {user && user.role === 'manager' && (<NavLink to="room-types" className="navLink">Room Types</NavLink>)}
      {user && <NavLink to="/logout" className="navLink">Logout</NavLink>}

    </nav>
  );
}

export default Navbar;
