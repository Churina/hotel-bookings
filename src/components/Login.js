import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import '../App.css';

function Login({ setUser }) {
  const [formInput, setFormInput] = useState({ email: '', password: '' });
  const [error, setError] = useState('');

  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await fetch(
        'http://localhost:8080/login',
        {
          method: 'POST',
          headers:
          {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${sessionStorage.getItem('token')}`
          },
          body: JSON.stringify(formInput)
        }
      );

      const data = await response.json();

      sessionStorage.setItem('token', data.token);

      // get the payload
      const user = JSON.parse(atob(data.token.split('.')[1]));

      sessionStorage.setItem('user', JSON.stringify({ email: user.sub, role: user.roles }));
      setUser({ email: user.sub, role: user.roles });
      navigate('/reservations');
    } catch (err) {
      if (!err.response) {
        setError('Invalid email or password');
      }
    }
  };

  // const handleChange = (event, input) => {
  //   setFormInput((prev) => ({...prev, [input]:event.target.value}));
  // }

  const handleChange = (event) => {
    const { name } = event.target;
    const { value } = event.target;
    setFormInput((prev) => ({ ...prev, [name]: value }));
  };

  return (
    <div className="login-form-container">
      <p className="error-message">{error}</p>
      <form title="Login" onSubmit={(e) => handleSubmit(e)}>
        <h2>Login</h2>
        <label htmlFor="email">
          Email:
          <input type="email" name="email" value={formInput.email} onChange={handleChange} />
        </label>

        <label htmlFor="password">
          Password:
          <input type="password" name="password" value={formInput.password} onChange={handleChange} />
        </label>

        <button type="submit">Login</button>
      </form>
    </div>
  );
}

export default Login;
