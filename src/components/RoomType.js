import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Spinner from './Spinner';

function RoomType() {
  const [room, setRoom] = useState({
    name: '', description: '', rate: '', active: true
  });
  const [error, setError] = useState('');
  const [inputError, setInputError] = useState({ name: '', rate: '' });

  const navigate = useNavigate();
  const params = useParams();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
  // if id exists in the URL, turn to "edit" mode
    if (params.id) {
      setLoading(true);
      const getRoomTypeData = async () => {
        try {
          const response = await fetch(
            `http://localhost:8080/room-types/${params.id}`,
            {
              method: 'GET',
              headers:
          {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${sessionStorage.getItem('token')}`
          }
            }
          );

          if (response.status === 404) {
            setLoading(false);
            setError(' 404 Page Not Found');
            navigate(`/room-types/${params.id}`);
          } else {
            const data = await response.json();
            setLoading(false);
            setRoom(data);
          }
        } catch (err) {
          if (!err.response) {
            setLoading(false);
            setError('Oops, something went wrong!');
          }
        }
      };
      getRoomTypeData();
    }
  }, [params.id, navigate]);

  const handleChange = (event) => {
    const { name } = event.target;
    const { value } = event.target;
    setRoom((prev) => ({ ...prev, [name]: value }));
  };

  const handleCheck = (event) => {
    setRoom((prev) => ({ ...prev, active: event.target.checked }));
  };

  const validateRoomForm = (input) => {
    const errors = {};

    if (input.name.length < 3) {
      errors.name = 'Must be at least 3 characters';
    }

    if (input.rate <= 0) {
      errors.rate = 'Must be number greater than zero';
    }
    return errors;
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const isValidated = validateRoomForm(room);

    // if the input has error, set and show error
    if (Object.keys(isValidated).length > 0) {
      setInputError(isValidated);
    } else if (params.id) {
      setLoading(true);
      // if the input has no error and id exists in the URL, update the form
      try {
        await fetch(
          `http://localhost:8080/room-types/${params.id}`,
          {
            method: 'PUT',
            headers:
          {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${sessionStorage.getItem('token')}`
          },
            body: JSON.stringify(room)
          }
        );
        setLoading(false);
        navigate('/room-types');
      } catch (err) {
        if (!err.response) {
          setLoading(false);
          setError('Oops, something went wrong!');
        }
      }
    } else {
      setLoading(true);
      // if the input has no error and id doesn't exists in the URL, create the form
      try {
        await fetch(
          'http://localhost:8080/room-types',
          {
            method: 'POST',
            headers:
          {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${sessionStorage.getItem('token')}`
          },
            body: JSON.stringify(room)
          }
        );

        setLoading(false);
        navigate('/room-types');
      } catch (err) {
        if (!err.response) {
          setLoading(false);
          setError('Oops, something went wrong!');
        }
      }
    }
  };

  return (
    <div className="reservation-roomType-form-container">

      <h1>
        {params.id ? 'Edit Room' : 'Create Room'}
        {' '}
      </h1>
      {error && <p className="error-message">{error}</p>}
      {loading && <Spinner pageStatus={loading} />}

      <form onSubmit={(e) => handleSubmit(e)} className="reservation-roomType-form">
        <label htmlFor="name">
          Room Type
          <input
            type="text"
            name="name"
            value={room.name}
            onChange={handleChange}
          />
        </label>
        <p className="errorMsg">{inputError.name}</p>

        <label htmlFor="description">
          Description
          <textarea
            rows="5"
            cols="22"
            className="textarea"
            name="description"
            value={room.description}
            onChange={handleChange}
          />
        </label>

        <label htmlFor="rate">
          Rate
          <input
            type="number"
            name="rate"
            value={room.rate}
            onChange={handleChange}
          />
        </label>
        <p className="errorMsg">{inputError.rate}</p>

        <div className="checkbox-container">
          <label htmlFor="roomTypeId">
            Active
            <input
              className="checkbox"
              type="checkbox"
              name="roomTypeId"
              checked={room.active}
              onChange={handleCheck}
            />
          </label>

        </div>
        <button type="submit">{params.id ? 'Update' : 'Create'}</button>
      </form>

    </div>
  );
}

export default RoomType;
