import React from 'react';

function NotFound() {
  return (
    <div style={{ textAlign: 'center', color: 'red' }}> 404 Page Not Found</div>
  );
}

export default NotFound;
