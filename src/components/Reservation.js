import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Spinner from './Spinner';

function Reservation() {
  const [reservationForm, setReservationForm] = useState({
    guestEmail: '', checkInDate: '', numberOfNights: '', roomTypeId: ''
  });
  const [rooms, setRooms] = useState([]);
  const [error, setError] = useState('');
  const [inputError, setInputError] = useState({
    guestEmail: '', checkInDate: '', numberOfNights: '', roomTypeId: ''
  });

  const navigate = useNavigate();
  const params = useParams();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
  // if id exists in the URL, turn to "edit" mode
    if (params.id) {
      setLoading(true);
      const getReservationAndRoomTypeData = async () => {
        try {
          // fetch reservations
          const response = await fetch(
            `http://localhost:8080/reservations/${params.id}`,
            {
              method: 'GET',
              headers:
            {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${sessionStorage.getItem('token')}`
            }
            }
          );

          if (response.status === 404) {
            setLoading(false);
            setError('404 Page Not Found');
            navigate(`/reservations/${params.id}`);
          } else {
            const data = await response.json();
            setLoading(false);
            setReservationForm(data);
          }

          // fetch room types
          const response1 = await fetch(
            'http://localhost:8080/room-types',
            {
              method: 'GET',
              headers:
            {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${sessionStorage.getItem('token')}`
            }
            }
          );

          const data1 = await response1.json();
          setLoading(false);
          setRooms(data1);
        } catch (err) {
          if (!err.response || !err.response1) {
            setLoading(false);
            setError('Oops, something went wrong!');
          }
        }
      };
      getReservationAndRoomTypeData();
    } else {
    // if no id exists in the URL, turn to "create" mode
      setLoading(true);
      const getRoomTypeData = async () => {
        try {
          // fetch room types
          const response2 = await fetch(
            'http://localhost:8080/room-types',
            {
              method: 'GET',
              headers:
            {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${sessionStorage.getItem('token')}`
            }
            }
          );

          const data2 = await response2.json();
          setLoading(false);
          setRooms(data2);
        } catch (err) {
          if (!err.response2) {
            setLoading(false);
            setError('Oops, something went wrong!');
          }
        }
      };
      getRoomTypeData();
    }
  }, [params.id, navigate]);

  const handleChange = (event) => {
    const { name } = event.target;
    const { value } = event.target;
    setReservationForm((prev) => ({ ...prev, [name]: value }));
  };

  const validateReservationForm = (values) => {
    const errors = {};
    const regexEmail = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    const regexDate = /^(0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d$/;

    if (!values.guestEmail) {
      errors.guestEmail = ' Please enter an email';
    } else if (!regexEmail.test(values.guestEmail)) {
      errors.guestEmail = 'Must be a valid email';
    }

    if (!values.checkInDate) {
      errors.checkInDate = 'Please enter a check-in date';
    } else if (!regexDate.test(values.checkInDate)) {
      errors.checkInDate = 'Date must be mm-dd-yyyy';
    }

    if (!values.numberOfNights) {
      errors.numberOfNights = 'Please enter number of nights';
    } else if (values.numberOfNights <= 0) {
      errors.numberOfNights = 'Must be number greater than zero';
    }

    if (reservationForm.roomTypeId === '') {
      errors.roomTypeId = 'Must select a room type';
    }
    return errors;
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const isValidated = validateReservationForm(reservationForm);

    // if the input has error, set and show error
    if (Object.keys(isValidated).length > 0) {
      setInputError(isValidated);
    } else if (params.id) {
      setLoading(true);
      // if the input has no error and id exists in the URL, update the form
      try {
        await fetch(
          `http://localhost:8080/reservations/${params.id}`,
          {
            method: 'PUT',
            headers:
            {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${sessionStorage.getItem('token')}`
            },
            body: JSON.stringify(reservationForm)
          }
        );
        setLoading(false);
        navigate('/reservations');
      } catch (err) {
        if (!err.response) {
          setLoading(false);
          setError('Oops, something went wrong!');
        }
      }
    } else {
      setLoading(true);
      // if the input has no error and id doesn't exists in the URL, create the form
      try {
        await fetch(
          'http://localhost:8080/reservations',
          {
            method: 'POST',
            headers:
            {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${sessionStorage.getItem('token')}`
            },
            body: JSON.stringify(reservationForm)
          }
        );

        setLoading(false);
        navigate('/reservations');
      } catch (err) {
        if (!err.response) {
          setLoading(false);
          setError('Oops, something went wrong!');
        }
      }
    }
  };

  const activeRooms = rooms.filter((room) => room.active);

  return (
    <div className="reservation-roomType-form-container">

      <h1>{params.id ? 'Edit Reservation' : 'Create Reservation'}</h1>
      {error && <p className="error-message">{error}</p>}
      {loading && <Spinner pageStatus={loading} />}

      <form onSubmit={(e) => handleSubmit(e)} className="reservation-roomType-form">
        <label htmlFor="guestEmail">
          Guest Email
          <input
            type="email"
            name="guestEmail"
            value={reservationForm.guestEmail}
            onChange={handleChange}
          />
        </label>
        <p className="errorMsg">{inputError.guestEmail}</p>

        <label htmlFor="checkInDate">
          Check-in Date
          <input
            type="text"
            name="checkInDate"
            value={reservationForm.checkInDate}
            onChange={handleChange}
          />
        </label>
        <p className="errorMsg">{inputError.checkInDate}</p>

        <label htmlFor="numberOfNights">
          Number of Nights
          <input
            type="number"
            name="numberOfNights"
            value={reservationForm.numberOfNights}
            onChange={handleChange}
          />
        </label>
        <p className="errorMsg">{inputError.numberOfNights}</p>

        <label htmlFor="roomTypeId">
          Room Type
          <select
            className="selcet-roomType"
            type="text"
            name="roomTypeId"
            value={reservationForm.roomTypeId}
            onChange={handleChange}
          >
            <option value="" hidden>Select one</option>
            {activeRooms.map((room) => (
              <option
                key={room.id}
                value={room.id}
              >
                {room.name}
              </option>
            ))}
          </select>
        </label>
        <p className="errorMsg">{inputError.roomTypeId}</p>
        <button type="submit">{params.id ? 'Update' : 'Create'}</button>
      </form>
    </div>
  );
}

export default Reservation;
