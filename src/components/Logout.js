import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const Logout = ({ setUser }) => {
  const navigate = useNavigate();
  useEffect(() => {
    setUser(null);
  }, [setUser]);
  sessionStorage.clear();
  return navigate('/');
};

export default Logout;
