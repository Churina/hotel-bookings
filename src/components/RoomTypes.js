import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Spinner from './Spinner';

function RoomTypes() {
  const [roomTypes, setRoomTypes] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  useEffect(() => {
    setLoading(true);
    const getRoomTypeData = async () => {
      try {
        const response = await fetch(
          'http://localhost:8080/room-types',
          {
            method: 'GET',
            headers:
          {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${sessionStorage.getItem('token')}`
          }
          }
        );

        const data = await response.json();
        setLoading(false);
        setRoomTypes(data);
      } catch (err) {
        if (!err.response) {
          setLoading(false);
          setError('Oops, something went wrong!');
        }
      }
    };
    getRoomTypeData();
  }, []);

  return (
    <div className="reservation-roomType-container">
      <h1>Room Types</h1>
      {error && <p className="error-message">{error}</p>}
      {!error && <Link to="/room-types/create"><button type="button">Create</button></Link>}
      {loading && <Spinner pageStatus={loading} />}
      {roomTypes.length > 0 && roomTypes.map((room) => (
        <div className="reservation-roomType-sub-container">
          <div className="reservation-roomType-context">
            <p>
              Room Type:
              {room.name}
            </p>
            <p>
              Description:
              {room.description}
            </p>
            <p>
              Rate:
              {room.rate}
            </p>
            <p>
              Status:
              {room.active ? 'Active' : 'Inactive'}
            </p>
          </div>
          <div className="button">
            <Link to={`/room-types/edit/${room.id}`}><button type="button">Edit</button></Link>
          </div>
        </div>
      ))}

    </div>
  );
}

export default RoomTypes;
