import React, { useState } from 'react';
import { Routes, Route } from 'react-router-dom';
import './App.css';
import Navbar from './components/Navbar';
import Login from './components/Login';
import Reservations from './components/Reservations';
import Reservation from './components/Reservation';
import RoomTypes from './components/RoomTypes';
import RoomType from './components/RoomType';
import Logout from './components/Logout';
import NotFound from './components/NotFound';
import Protected from './components/Protected';

function App() {
  const [user, setUser] = useState(JSON.parse(sessionStorage.getItem('user')));

  return (
    <div>
      <Navbar user={user} />
      <Routes>
        <Route path="/" element={<Login setUser={setUser} />} />
        <Route
          path="/reservations"
          element={(
            <Protected loggedIn={user}>
              <Reservations />
            </Protected>
      )}
        />
        <Route
          path="/reservations/create"
          element={(
            <Protected loggedIn={user}>
              <Reservation />
            </Protected>
      )}
        />
        <Route
          path="/reservations/edit/:id"
          element={(
            <Protected loggedIn={user}>
              <Reservation />
            </Protected>
      )}
        />
        <Route
          path="/room-types"
          element={(
            <Protected loggedIn={user}>
              <RoomTypes />
            </Protected>
      )}
        />
        <Route
          path="/room-types/create"
          element={(
            <Protected loggedIn={user}>
              <RoomType />
            </Protected>
      )}
        />
        <Route
          path="/room-types/edit/:id"
          element={(
            <Protected loggedIn={user}>
              <RoomType />
            </Protected>
      )}
        />
        <Route
          path="/logout"
          element={(
            <Protected loggedIn={user}>
              <Logout setUser={setUser} />
            </Protected>
      )}
        />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </div>
  );
}

export default App;
